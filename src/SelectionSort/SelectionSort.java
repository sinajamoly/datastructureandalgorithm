package SelectionSort;

public class SelectionSort {
    private int[] numbers;

    public SelectionSort(int[] numbers)
    {
        this.numbers = numbers;
    }

    public int[] sort()
    {
        for (int i = 0; i < numbers.length; i++) {
            int biggestIndex = 0;

            int targetIndex = numbers.length - ( i + 1 );
            for (int j = 0; j < numbers.length - i; j++) {
                if (numbers[biggestIndex] < numbers[j]) {
                    biggestIndex = j;
                }
            }

            int temp = numbers[targetIndex];
            numbers[targetIndex] = numbers[biggestIndex];
            numbers[biggestIndex] = temp;
        }

        return this.numbers;
    }
}
