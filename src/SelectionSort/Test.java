package SelectionSort;

import BubbleSort.BubbleSort;
import Printer.PrintArray;

public class Test {
    public static void main(String[] arg)
    {
        int[] numbers = {5, 2, 3, -10, 33, 12, 1};

        SelectionSort selectionSort = new SelectionSort(numbers);

        int[] sortedNumbers = selectionSort.sort();

        PrintArray printer = new PrintArray(sortedNumbers);

        printer.print();
    }
}
