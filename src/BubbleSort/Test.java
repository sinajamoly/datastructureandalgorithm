package BubbleSort;

import Printer.PrintArray;

public class Test {
    public static void main(String[] arg)
    {
        int[] numbers = {5, 2, 3, -10, 33, 12, 1};

        BubbleSort bubbleSort = new BubbleSort(numbers);

        int[] sorterNumbers = bubbleSort.bubbleSort();

        PrintArray printer = new PrintArray(sorterNumbers);

        printer.print();
    }
}
