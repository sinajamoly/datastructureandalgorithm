package BubbleSort;

public class BubbleSort {
    private int[] numbers;

    BubbleSort(int[] numbers)
    {
        this.numbers = numbers;
    }
    
    public int[] bubbleSort()
    {
        int rep = 0;

        for (int i = 0; i < numbers.length; i++) {
            int biggestIndex = i;
            for (int j = 0; j < numbers.length - (i + 1); j++) {
                if (numbers[j] > numbers[j+1]) {
                    int tempNumber = numbers[j + 1];
                    numbers[j + 1 ] = numbers[j];
                    numbers[j] = tempNumber;
                }
                rep = rep + 1;
            }
        }

        System.out.println("number of repetitions is: " + rep);

        return this.numbers;
    }

    public int[] getNumbers()
    {
        return this.numbers;
    }
}