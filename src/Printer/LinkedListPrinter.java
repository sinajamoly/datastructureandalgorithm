package Printer;

import LinkedList.LinkedList;
import LinkedList.Node;

public class LinkedListPrinter implements Printer{
    private LinkedList linkedList;

    public LinkedListPrinter(LinkedList linkedList)
    {
        this.linkedList = linkedList;
    }

    @Override
    public void print() {
        Node current = this.linkedList.getHead();
        String linkedListString = "{ ";

        while (current != null) {
            linkedListString += current.toString();

            current = current.getNext();
        }

        linkedListString += " }";

        System.out.println(linkedListString);
    }
}
