package Printer;

public class PrintArray implements Printer {
    int[] numbers;

    public PrintArray(int[] numbers){
        this.numbers = numbers;
    }

    @Override
    public void print() {
        for (int number : this.numbers) {
            System.out.println(number);
        }
    }
}
