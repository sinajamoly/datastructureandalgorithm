package LinkedList;

public class Node {
    private Node next;

    public String data;

    public Node(String data)
    {
        this.data = data;
    }

    public Node getNext()
    {
        return this.next;
    }

    public Node setNext(Node nextNode)
    {
        this.next = nextNode;

        return this;
    }

    public String getData()
    {
        return this.data;
    }

    public String toString()
    {
        return " data: " + this.data;
    }
}
