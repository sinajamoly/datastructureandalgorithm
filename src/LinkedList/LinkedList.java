package LinkedList;

public class LinkedList {
    public Node head;

    public LinkedList(String data)
    {
        this.head = new Node(data);
    }

    public Node getHead()
    {
        return this.head;
    }

    public void insertAtHead(String data)
    {
        Node newNode = new Node(data);
        newNode.setNext(this.head);
        this.head = newNode;
    }

    public int getSize()
    {
        Node current = this.head;
        int counter = 0;

        while (current != null)
        {
            counter += 1;

            current = current.getNext();
        }

        return counter;
    }

    public void deleteFromHead()
    {
        this.head = this.head.getNext();
    }

    public boolean search(String data)
    {
        Node current = this.head;

        while (current != null) {
            if (current.getData() == data) {
                return true;
            }

            current = current.getNext();
        }

        return false;
    }
}
