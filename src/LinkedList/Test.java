package LinkedList;

import Printer.LinkedListPrinter;

public class Test {
    public static void main(String[] arg)
    {
        LinkedList linkedList = new LinkedList("1");
        linkedList.insertAtHead("2");
        linkedList.insertAtHead("3");
        linkedList.insertAtHead("4");

        LinkedListPrinter linkedListPrinter = new LinkedListPrinter(linkedList);
        linkedListPrinter.print();

        System.out.println(linkedList.getSize());

        linkedList.deleteFromHead();
        linkedListPrinter.print();

        System.out.println(linkedList.search("55"));

    }
}

